﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Input;
using SuperHero.Entities;
using SuperHero.Wpf.Commands;
namespace SuperHero.Wpf.ViewModel
{
    public class SuperHeroMaintenanceViewModel : ObservableObject
    {
        #region properties
        private Guid _id;
        private string _superHeroName;
        private string _identityFirstName;
        private string _identityLastName;
        private string _superHeroGroup;
        private DateTime _createdUtc;
        private DateTime _updatedUtc;
        private List<SuperHeroEntity> _allHeroes;
        private string _backgroundColorOne;
        private string _backgroundColorTwo;
        private string _superHeroPhotoLink;
        private SuperHeroEntity _currentHero;

        public Guid Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        public string SuperHeroName
        {
            get => _superHeroName;
            set
            {
                _superHeroName = value;
                OnPropertyChanged($"SupherHeroName");
            }
        }

        public string IdentityFirstName
        {
            get => _identityFirstName;
            set
            {
                _identityFirstName = value;
                OnPropertyChanged();
            }
        }

        public string IdentityLastName
        {
            get => _identityLastName;
            set
            {
                _identityLastName = value;
                OnPropertyChanged();
            }
        }

        public string SuperHeroGroup
        {
            get => _superHeroGroup;
            set
            {
                _superHeroGroup = value;
                OnPropertyChanged();
            }
        }

        public string SuperHeroPhotoLink
        {
            get => _superHeroPhotoLink;
            set
            {
                _superHeroPhotoLink = value;
                OnPropertyChanged($"SuperHeroPhotoLink");
            }
        }

        public string BackgroundColorOne
        {
            get => _backgroundColorOne;
            set
            {
                _backgroundColorOne = value;
                OnPropertyChanged($"BackgroundColorOne");
            }
        }

        public string BackgroundColorTwo
        {
            get => _backgroundColorTwo;
            set
            {
                _backgroundColorTwo = value;
                OnPropertyChanged($"BackgroundColorTwo");
            }
        }

        public DateTime CreatedUtc
        {
            get => _createdUtc;
            set
            {
                _createdUtc = value;
                OnPropertyChanged($"CreatedUtc");
            }
        }

        public DateTime UpdatedUtc
        {
            get => _updatedUtc;
            set
            {
                _updatedUtc = value;
                OnPropertyChanged();
            }
        }

        public SuperHeroEntity CurrentHero 
        {
            get => _currentHero;
            set
            {
                _currentHero = value;
                OnPropertyChanged($"CurrentHero");
            }
        }

#endregion

        public SuperHeroMaintenanceViewModel()
        {
            GetSuperHeroes();
        }

        private ICommand _previousHeroCommand;

        public ICommand PreviousHeroCommand
        {
            get
            {
                if (_previousHeroCommand == null)
                {
                    _previousHeroCommand = new RelayCommand(MovePreviousHero);
                }
                return _previousHeroCommand;
            }
        }

        private async void GetSuperHeroes()
        {
            //ToDo: Change this to a singleton...see patterns and practices as well as http://www.nimaara.com/2016/11/01/beware-of-the-net-httpclient/
            var endpoint = new Uri("http://localhost:57894/");
           
            using (var client = new HttpClient())
                {
                    client.BaseAddress = endpoint;
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = await client.GetAsync("api/SuperHero").ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                       _allHeroes = await response.Content.ReadAsAsync<List<SuperHeroEntity>>();
                    }
                   
                }

            if (_allHeroes != null && _allHeroes.Count > 0)
            {
                CurrentHero = _allHeroes[0];
            }

        }

        public ICommand NextHeroCommand => new RelayCommand(MoveNextHero);

        private void MoveNextHero()
        {
            if (_allHeroes != null)
            {
                //get index 
                var index = _allHeroes.FindIndex(x => x.Id.Equals(_currentHero.Id));
                if (index == _allHeroes.Count -1)
                {
                    CurrentHero = _allHeroes[0];
                }
                else
                {
                    index++;
                    CurrentHero = _allHeroes[index];
                }
            }
        }

        private void MovePreviousHero()
        {
            if (_allHeroes != null)
            {
                //get index 
                var index = _allHeroes.FindIndex(x => x.Id.Equals(_currentHero.Id));
                if (index > 0)
                {
                    index--;
                    CurrentHero = _allHeroes[index];
                }
                else
                {
                    CurrentHero = _allHeroes[_allHeroes.Count - 1];
                }
            }
        }
    }
}
