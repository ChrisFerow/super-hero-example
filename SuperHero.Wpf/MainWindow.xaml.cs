﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SuperHero.Wpf.UserControls;
using SuperHero.Wpf.Windows;

namespace SuperHero.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void HeroMaintenance_OnClick(object sender, RoutedEventArgs e)
        {
           // DetailControl.Children.Clear();//Clearing Child Elements in Grid Panel
            //var heroControl = new HeroMaintenance();
            var heroPage = new SuperHeroMaintenance(this);
            //NavigationFrame.Navigate(heroPage);
            NavigationFrame.NavigationService.Navigate(heroPage);
            //DetailControl.Children.Add(heroControl);
        }
    }
}
