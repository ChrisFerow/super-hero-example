﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperHero.Wpf.Windows
{
    /// <summary>
    /// Interaction logic for SuperHeroMaintenance.xaml
    /// </summary>
    public partial class SuperHeroMaintenance : Page
    {
        private MainWindow _main;
        public SuperHeroMaintenance(MainWindow mainWindow)
        {
            _main = mainWindow;
            InitializeComponent();
        }

        private void Close_OnClick(object sender, RoutedEventArgs e)
        {
            //((Frame)this.Parent).NavigationService.GoBack();
            _main.NavigationFrame.Content = string.Empty;
        }
    }
}
