﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using SuperHero.Entities;
using SuperHero.Repository.Interface;

namespace SuperHero.Repository
{
    public class SuperHeroRepository : BaseRepository, IRepository<SuperHeroEntity>
    {
        public async Task<bool> Create(SuperHeroEntity item)
        {
            try
            {
                await WithConnection(async c => {

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@HeroId", item.Id, DbType.Guid);
                    parameters.Add("@FirstName", item.IdentityFirstName);
                    parameters.Add("@LastName", item.IdentityLastName);
                    parameters.Add("@HeroName", item.SuperHeroName);
                    parameters.Add("@GroupId", item.SuperHeroGroup, DbType.Guid);

                    await c.ExecuteAsync(
                        sql: "[dbo].[SuperHero_AddHero]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                });
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<SuperHeroEntity>> FindAll()
        {
            try
            {
                return await WithConnection(async c => {
                    // Here's all the same data access code,
                    // albeit now it's async, and nicely wrapped
                    // in this handy WithConnection() call.
                    var configurations = await c.QueryAsync<SuperHeroEntity>(
                        sql: "[dbo].[SuperHero_GetHeroes]",
                        commandType: CommandType.StoredProcedure);
                    return configurations;

                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<SuperHeroEntity> Find(Guid id)
        {
            try
            {
                return await WithConnection(async c => {

                    // Here's all the same data access code,
                    // albeit now it's async, and nicely wrapped
                    // in this handy WithConnection() call.
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@HeroId", id, DbType.Guid);
                    var people = await c.QueryAsync<SuperHeroEntity>(
                        sql: "[dbo].[SuperHero_FindById]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                    return people.FirstOrDefault();

                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            try
            {
                return await WithConnection(async c => {

                    // Here's all the same data access code,
                    // albeit now it's async, and nicely wrapped
                    // in this handy WithConnection() call.
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@HeroId", id, DbType.Guid);
                    await c.QueryAsync<SuperHeroGroup>(
                        sql: "[dbo].[SuperHero_DeleteHero]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                    return true;

                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> Update(SuperHeroEntity item)
        {
            try
            {
                await WithConnection(async c => {

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@HeroId", item.Id, DbType.Guid);
                    parameters.Add("@FirstName", item.IdentityFirstName);
                    parameters.Add("@LastName", item.IdentityLastName);
                    parameters.Add("@HeroName", item.SuperHeroName);
                    parameters.Add("@GroupId", item.SuperHeroGroup, DbType.Guid);

                    await c.ExecuteAsync(
                        sql: "[dbo].[SuperHero_UpdateHero]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                });
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
