﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SuperHero.Repository
{
    public class BaseRepository
    {
       // protected IDbConnection Connection;

        public string ConnectionString { get; set; } 


        public BaseRepository()
        {
            //var connectionString = string.Empty;
            try
            {

                //Todo: add this to configuration file 
                ConnectionString =
                    "Server=SurfaceFro;Initial Catalog=SuperHero;Integrated Security=true; MultipleActiveResultSets = False;  Connection Timeout=30;";
                //Connection = new SqlConnection(ConnectionString);
            }
            catch (Exception e)
            {
                throw new Exception($"There was an issue connecting to {ConnectionString}.  The error is {e}");
            }
        }

        // use for buffered queries that return a type
        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL timeout", ex);
            }
            catch (SqlException ex)
            {
                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL exception (not a )", ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL timeout", ex);
            }
            catch (SqlException ex)
            {
                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL exception (not a timeout)", ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process, string connectionString)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL timeout", ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(
                    $"{GetType().FullName}.WithConnection() experienced a SQL exception (not a timeout)", ex);
            }
        }


        //protected class EnittyOneToManyMapper<TP, TC, TPk>
        //{
        //    private readonly IDictionary<TPk, TP> _lookup = new Dictionary<TPk, TP>();

        //    public Action<TP, TC> AddChildAction { get; set; }

        //    public Func<TP, TPk> ParentKey { get; set; }
           

        //    public virtual TP Map(TP parent, TC child)
        //    {
        //        TP entity;
        //        var found = true;
        //        var primaryKey = ParentKey(parent);

        //        if (!_lookup.TryGetValue(primaryKey, out entity))
        //        {
        //            _lookup.Add(primaryKey, parent);
        //            entity = parent;
        //            found = false;
        //        }

        //        AddChildAction(entity, child);

        //        return !found ? entity : default(TP);

        //    }
        //}
    }
}
