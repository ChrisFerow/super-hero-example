﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using SuperHero.Entities;
using SuperHero.Repository.Interface;

namespace SuperHero.Repository
{
    public class SuperHeroGroupRepository : BaseRepository,  IRepository<SuperHeroGroup>
    {
        public async Task<bool> Create(SuperHeroGroup item)
        {
            try
            {
                await WithConnection(async c => {

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@GroupId", item.Id, DbType.Guid);
                    parameters.Add("@GroupName", item.SuperHeroGroupName);
                  
                    await c.ExecuteAsync(
                        sql: "[dbo].[SuperHeroGroups_AddGroup]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                });
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IEnumerable<SuperHeroGroup>> FindAll()
        {
            try
            {
                return await WithConnection(async c => {

                    // Here's all the same data access code,
                    // albeit now it's async, and nicely wrapped
                    // in this handy WithConnection() call
                    return await c.QueryAsync<SuperHeroGroup>(
                        sql: "[dbo].[SuperHeroGroup_FindAll]",
                        
                        commandType: CommandType.StoredProcedure);
                });
                
        //return await sqlConnection.QueryAsync<SuperHeroGroup, SuperHeroEntity, SuperHeroGroup>
        //(
        //    @"SELECT 
        //        Groups.Id, 
        //        Groups.SuperHeroGroupName, 
        //        Groups.CreatedUtc,
        //        Groups.UpdatedUtc,
        //        Hero.IdentityFirstName,
        //        Hero.IdentityLastName,
        //        Hero.SuperHeroName
        //    From
        //        SuperHeroGroup Groups
        //     Left Outer Join
        //        SuperHero Hero
        //    On 
        //        Groups.Id = Hero.SuperHeroGroupId",
        //    (group, hero) =>
        //    {
        //        //group.SuperHeroes.Add(hero);
        //        hero.SuperHeroGroup = group;
        //        return group;
        //    },
        //    splitOn: "SuperHeroName"

        //);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<SuperHeroGroup> Find(Guid id)
        {
            try
            {
                return await WithConnection(async c => {

                    // Here's all the same data access code,
                    // albeit now it's async, and nicely wrapped
                    // in this handy WithConnection() call.
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@GroupId", id, DbType.Guid);
                    var people = await c.QueryAsync<SuperHeroGroup>(
                        sql: "[dbo].[SuperHeroGroup_FindById]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                    return people.FirstOrDefault();

                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> Delete(Guid id)
        {
            try
            {
                return await WithConnection(async c => {

                    // Here's all the same data access code,
                    // albeit now it's async, and nicely wrapped
                    // in this handy WithConnection() call.
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@GroupId", id, DbType.Guid);
                    await c.QueryAsync<SuperHeroGroup>(
                        sql: "[dbo].[SuperHeroGroups_DeleteGroup]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                    return true;

                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> Update(SuperHeroGroup item)
        {
            try
            {
                await WithConnection(async c => {

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@GroupId", item.Id, DbType.Guid);
                    parameters.Add("@GroupName", item.SuperHeroGroupName);

                    await c.ExecuteAsync(
                        sql: "[dbo].[SuperHeroGroups_UpdateGroup]",
                        param: parameters,
                        commandType: CommandType.StoredProcedure);
                });
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
