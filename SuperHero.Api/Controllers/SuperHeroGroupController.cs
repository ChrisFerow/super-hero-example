﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuperHero.Entities;
using SuperHero.Repository.Interface;

namespace SuperHero.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/SuperHeroGroup")]
    public class SuperHeroGroupController : Controller
    {
        private readonly IRepository<SuperHeroGroup> _superHeroGroupRepository;
        public SuperHeroGroupController(IRepository<SuperHeroGroup> superHeroGroupRepository)
        {
            _superHeroGroupRepository = superHeroGroupRepository;
        }

        [HttpGet]
        public Task<IEnumerable<SuperHeroGroup>> FindAllGroups()
        {
            return _superHeroGroupRepository.FindAll();
        }

        
        // GET api/user/5  
        [HttpGet("{id}")]
        public Task<SuperHeroGroup> FindGroupById(Guid id)
        {
            return _superHeroGroupRepository.Find(id);

        }
        // POST api/user  
        [HttpPost]
        public Task<bool> AddGroup([FromBody]SuperHeroGroup group)
        {
            return _superHeroGroupRepository.Create(group);
        }
        // PUT api/user/5  
        [HttpPut("{groupId}")]
        public Task<bool> UpdateGroup(Guid groupId, [FromBody]SuperHeroGroup group)
        {
            return _superHeroGroupRepository.Update(group);
        }
        // DELETE api/user/5  
        [HttpDelete("{id}")]
        public Task<bool> DeleteGroup(Guid id)
        {
            return _superHeroGroupRepository.Delete(id);

        }
    }
}