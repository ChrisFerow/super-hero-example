﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperHero.Entities;
using SuperHero.Repository.Interface;

namespace SuperHero.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/SuperHero")]
    public class SuperHeroController : Controller
    {
        private readonly IRepository<SuperHeroEntity> _superHeroRepository;
        public SuperHeroController(IRepository<SuperHeroEntity> superHeroRepository)
        {
            _superHeroRepository = superHeroRepository;
        }

        [HttpGet]
        public Task<IEnumerable<SuperHeroEntity>> FindAllHeroes()
        {
            return _superHeroRepository.FindAll();
        }

        // GET api/user/5  
        [HttpGet("{id}")]
        public Task<SuperHeroEntity> FindHeroById(Guid id)
        {
            return _superHeroRepository.Find(id);

        }
        // POST api/user  
        [HttpPost]
        public Task<bool> AddHero([FromBody]SuperHeroEntity hero)
        {
            return _superHeroRepository.Create(hero);
        }
        // PUT api/user/5  
        [HttpPut("{groupId}")]
        public Task<bool> UpdateHero(Guid groupId, [FromBody]SuperHeroEntity group)
        {
            return _superHeroRepository.Update(group);
        }
        // DELETE api/user/5  
        [HttpDelete("{id}")]
        public Task<bool> DeleteHero(Guid id)
        {
            return _superHeroRepository.Delete(id);

        }
    }
}