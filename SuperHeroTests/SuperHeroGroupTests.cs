﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Ploeh.AutoFixture;
using SuperHero.Api.Controllers;
using SuperHero.Entities;
using SuperHero.Repository.Interface;
using Xunit;

namespace SuperHeroTests
{
    public class SuperHeroGroupTests
    {
        private Mock<IRepository<SuperHeroGroup>> _mockSuperHeroGroupRepository;
        private List<SuperHeroGroup> _superHeroGroups;
        private SuperHeroGroupController controller;

        public SuperHeroGroupTests()
        {
            // Set up Repository basics
            var fixture = new Fixture();
            _superHeroGroups = fixture.Freeze<List<SuperHeroGroup>>();

            //Set up Number of SuperHeroGroups
            _mockSuperHeroGroupRepository = new Mock<IRepository<SuperHeroGroup>>(MockBehavior.Strict);
            _mockSuperHeroGroupRepository.Setup(x => x.FindAll()).ReturnsAsync(_superHeroGroups);

            // Allows us to test saving a product
            _mockSuperHeroGroupRepository.Setup(x => x.Create(It.IsAny<SuperHeroGroup>())).ReturnsAsync(true);
            _mockSuperHeroGroupRepository.Setup(x => x.Find(It.IsAny<Guid>())).ReturnsAsync((Guid id) => _superHeroGroups.Single(w => w.Id == id));

            //This is over kill as we do not need to add the update logic as that will be in another repository that will not be tested.
            //I wanted to show an example though in the event we did want to do something further with the data.
            _mockSuperHeroGroupRepository.Setup(x => x.Update(It.IsAny<SuperHeroGroup>())).ReturnsAsync(
                (SuperHeroGroup target) =>
                {
                    var original = _superHeroGroups.Single(q => q.Id == target.Id);

                    if (original == null)
                    {
                        return false;
                    }
                    original.SuperHeroGroupName = target.SuperHeroGroupName;
                    original.UpdatedUtc = DateTime.Now;
                  
                    return true;
                });

            _mockSuperHeroGroupRepository.Setup(x => x.Delete(It.IsAny<Guid>())).ReturnsAsync(
                (Guid superHeroGroupId) =>
                {
                    var original = _superHeroGroups.Single(q => q.Id == superHeroGroupId);

                    if (original == null)
                    {
                        return false;
                    }
                    _superHeroGroups.Remove(original);

                    return true;
                }
                );

            controller = new SuperHeroGroupController(_mockSuperHeroGroupRepository.Object);
        }

        [Fact]
        public void GetAllGroupsTest()
        {
            _mockSuperHeroGroupRepository = new Mock<IRepository<SuperHeroGroup>>(MockBehavior.Strict);
            _mockSuperHeroGroupRepository.Setup(x => x.FindAll()).ReturnsAsync(MockGroupData);

            var groupController = new SuperHeroGroupController(_mockSuperHeroGroupRepository.Object);

            var result = groupController.FindAllGroups();

            var model = Assert.IsAssignableFrom<Task<IEnumerable<SuperHeroGroup>>>(result);
            Assert.Equal(MockGroupData().Count, model.Result.Count());
        }

        [Fact]
        public async void GetAllGroupsWithFixtureData()
        {
            var result = await controller.FindAllGroups();

            //Did we get back a list of courts
            Assert.IsType<List<SuperHeroGroup>>(result);

            //Does this list of courts match the number in the repository
            //Assert.Equal(result.Count(), _courts.Count());
        }

        [Fact]
        public async void AddGroupHappyPath()
        {
            var fixture = new Fixture();

            var item = fixture.Create<SuperHeroGroup>();

            //Act
            var result = await controller.AddGroup(item);
            //Assert
            Assert.True(result);
        }

        [Fact]
        public async void UpdateGroupHappyPath()
        {
            var fixture = new Fixture();

            var randomData = fixture.Create<SuperHeroGroup>();

            //Get the id of a super hero
            var groupId = _superHeroGroups[0].Id;
            //Replace the web id
            randomData.Id = groupId;

            var results = await controller.UpdateGroup(groupId, randomData);
            Assert.True(results);
        }

        [Fact]
        public async void DeleteGroupHappyPath()
        {
            //Get the id of a court
            var beforeCount = _superHeroGroups.Count();
            var groupId = _superHeroGroups[0].Id;

            var results = await controller.DeleteGroup(groupId);

            //Once again this is overkill because in our test repository data can be removed, but we are not
            //persisting the data from test to test.
            Assert.Equal(beforeCount - 1, _superHeroGroups.Count());
            Assert.True(results);
        }

        private List<SuperHeroGroup> MockGroupData()
        {
            return new List<SuperHeroGroup>()
            {
                new SuperHeroGroup()
                {
                    SuperHeroGroupName = "Justice League",
                    Id = Guid.NewGuid()
                },
                new SuperHeroGroup()
                {
                    SuperHeroGroupName = "Avengers",
                    Id = Guid.NewGuid()
                },
                new SuperHeroGroup()
                {
                    SuperHeroGroupName = "Fantastic Four",
                    Id = Guid.NewGuid()
                }
            };
        }

    }
}
