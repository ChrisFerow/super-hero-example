﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Ploeh.AutoFixture;
using SuperHero.Api.Controllers;
using SuperHero.Entities;
using SuperHero.Repository.Interface;
using Xunit;

namespace SuperHeroTests
{
    public class SuperHeroTests
    {
        private Mock<IRepository<SuperHeroEntity>> _mockSuperHeroRepository;
        private List<SuperHeroEntity> _superHero;
        private SuperHeroController controller;

        public SuperHeroTests()
        {
            var fixture = new Fixture();
            _superHero = fixture.Freeze<List<SuperHeroEntity>>();

            //Set up Number of SuperHeroEntitys
            _mockSuperHeroRepository = new Mock<IRepository<SuperHeroEntity>>(MockBehavior.Strict);
            _mockSuperHeroRepository.Setup(x => x.FindAll()).ReturnsAsync(_superHero);

            // Allows us to test saving a product
            _mockSuperHeroRepository.Setup(x => x.Create(It.IsAny<SuperHeroEntity>())).ReturnsAsync(true);
            _mockSuperHeroRepository.Setup(x => x.Find(It.IsAny<Guid>())).ReturnsAsync((Guid id) => _superHero.Single(w => w.Id == id));

            //This is over kill as we do not need to add the update logic as that will be in another repository that will not be tested.
            //I wanted to show an example though in the event we did want to do something further with the data.
            _mockSuperHeroRepository.Setup(x => x.Update(It.IsAny<SuperHeroEntity>())).ReturnsAsync(
                (SuperHeroEntity target) =>
                {
                    var original = _superHero.Single(q => q.Id == target.Id);

                    if (original == null)
                    {
                        return false;
                    }
                    original.SuperHeroName = target.SuperHeroName;
                    original.IdentityFirstName = target.IdentityFirstName;
                    original.IdentityLastName = target.IdentityLastName;
                    original.SuperHeroGroup = target.SuperHeroGroup;
                    original.UpdatedUtc = DateTime.Now;
                   
                    return true; 
                });

            _mockSuperHeroRepository.Setup(x => x.Delete(It.IsAny<Guid>())).ReturnsAsync(
                (Guid superHeroEntityId) =>
                {
                    var original = _superHero.Single(q => q.Id == superHeroEntityId);

                    if (original == null)
                    {
                        return false;
                    }
                    _superHero.Remove(original);

                    return true;
                }
                );

            controller = new SuperHeroController(_mockSuperHeroRepository.Object);
        }

        [Fact]
        public void GetAllHeroesTest()
        {
            _mockSuperHeroRepository = new Mock<IRepository<SuperHeroEntity>>(MockBehavior.Strict);
            _mockSuperHeroRepository.Setup(x => x.FindAll()).ReturnsAsync(MockGroupData);

            var heroController = new SuperHeroController(_mockSuperHeroRepository.Object);
            
            var result = heroController.FindAllHeroes();

            var model = Assert.IsAssignableFrom<Task<IEnumerable<SuperHeroEntity>>>(result);
            Assert.Equal(MockGroupData().Count, model.Result.Count());
        }

        [Fact]
        public async void GetAllHeroesWithFixtureData()
        {
            var result = await controller.FindAllHeroes();

            //Did we get back a list of courts
            Assert.IsType<List<SuperHeroEntity>>(result);

            //Does this list of courts match the number in the repository
            //Assert.Equal(result.Count(), _courts.Count());
        }

        [Fact]
        public async void AddHeroHappyPath()
        {
            var fixture = new Fixture();

            var item = fixture.Create<SuperHeroEntity>();

            //Act
            var result = await controller.AddHero(item);
            //Assert
            Assert.True(result);
        }

        [Fact]
        public async void UpdateHeroHappyPath()
        {
            var fixture = new Fixture();

            var randomData = fixture.Create<SuperHeroEntity>();

            //Get the id of a super hero
            var heroId = _superHero[0].Id;
            //Replace the web id
            randomData.Id = heroId;

            var results = await controller.UpdateHero(heroId, randomData);
            Assert.True(results);
        }

        [Fact]
        public async void DeleteHeroHappyPath()
        {
            //Get the id of a hero
            var beforeCount = _superHero.Count();
            var courtId = _superHero[0].Id;

            var results = await controller.DeleteHero(courtId);

            //Once again this is overkill because in our test repository data can be removed, but we are not
            //persisting the data from test to test.
            Assert.Equal(beforeCount - 1, _superHero.Count());
            Assert.True(results);
        }

        private List<SuperHeroEntity> MockGroupData()
        {
            var groupA = Guid.NewGuid();
            var groupB = Guid.NewGuid();
            return new List<SuperHeroEntity>()
            {
                new SuperHeroEntity()
                {
                    SuperHeroName = "Captain America",
                    IdentityFirstName = "Steve",    
                    IdentityLastName = "Rodgers",
                    SuperHeroGroup = groupA,
                    Id = Guid.NewGuid()
                },
                new SuperHeroEntity()
                {
                    SuperHeroName = "Spiderman",
                    IdentityFirstName = "Peter",
                    IdentityLastName = "Parker",
                    SuperHeroGroup = groupA,
                    Id = Guid.NewGuid()
                },
                new SuperHeroEntity()
                {
                    SuperHeroName = "Mr. Fantastic",
                    IdentityFirstName = "Reed",
                    IdentityLastName = "Richards",
                    SuperHeroGroup = groupB,
                    Id = Guid.NewGuid()
                }
            };
        }
    }
}
