﻿using System;

namespace SuperHero.Entities
{
    public class SuperHeroGroups
    {
        public Guid SuperHeroGroupId { get; set; }
        public string SuperHeroGroupName { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime UpdatedUtc { get; set; }
    }
}
