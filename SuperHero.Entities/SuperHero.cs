﻿using System;
using System.Reflection;


namespace SuperHero.Entities
{
    public class SuperHeroEntity
    {
        public Guid Id { get; set; }
        public string SuperHeroName { get; set; }
        public string IdentityFirstName { get; set; }
        public string IdentityLastName { get; set; }
        public Guid SuperHeroGroup { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime UpdatedUtc { get; set; }
        public string SuperHeroPhotoLink { get; set; }
        public string BackgroundColorOne { get; set; }
        public string BackgroundColorTwo { get; set; }
    }
}
