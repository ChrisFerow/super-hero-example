﻿using System;
using System.Collections.Generic;

namespace SuperHero.Entities
{
    public class SuperHeroGroup
    {
        public Guid Id { get; set; }
        public string SuperHeroGroupName { get; set; }
        public DateTime CreatedUtc { get; set; }
        public DateTime UpdatedUtc { get; set; }

        //public List<SuperHeroEntity> SuperHeroes;

        //public SuperHeroGroup()
        //{
        //    SuperHeroes = new List<SuperHeroEntity>();
        //}
    }
}
