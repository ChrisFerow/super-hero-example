﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuperHero.Repository.Interface
{
    public interface IRepository<T>
    {
        Task<bool> Create(T item);
        Task<IEnumerable<T>> FindAll();
        Task<T> Find(Guid id);
        Task<bool> Delete(Guid id);
        Task<bool> Update(T item);
    }
}
